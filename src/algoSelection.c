void selection(int *t, int taille)
{
	int i, mini, j, x;
	for (i = 0; i < taille - 1; i++)
	{
		mini = i;
		for (j = i + 1; j < taille; j++)
			if (t[j] < t[mini])
				mini = j;
		x = t[i];
		t[i] = t[mini];
		t[mini] = x;
	}
}
